($(document).on("ready", function() {
    var up = $(".to_up");

    $("div[data-src],a[data-src]").each(function() {
        setElementBackground.apply(this);
    });

    function setElementBackground() {
        var elem = $(this);
        var src = elem.data("src");
        elem.css("background-image", `url(${src})`);
    }

    // $(".switcher span").on("click", function() {
    //     var span = $(this);
    //     if (!span.hasClass("_selected")) {
    //         span.parent().find("span").removeClass("_selected");
    //         span.addClass("_selected");
    //         $(".services,.advantages").removeClass("_selected");
    //         $("." + span.data("toggle")).addClass("_selected");
    //     }
    // })

    if ($("#about-slider").length != 0) {
        var aboutSlider = $("#about-slider").lightSlider({
            controls: false,
            auto: true,
            loop: true,
            pager: false,
            item: 3,
            pause: 10000,
            slideMargin: 30,
            responsive: [{
                breakpoint: 768,
                settings: {
                    item: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 1
                }
            }]
        });

        $(".slider-control._prev[data-switch='#about-slider']").click(function() {
            aboutSlider.goToPrevSlide();
        });

        $(".slider-control._next[data-switch='#about-slider']").click(function() {
            aboutSlider.goToNextSlide();
        });
    }

    if ($("#certificates-slider").length != 0) {
        var certificatesSlider = $("#certificates-slider").lightSlider({
            controls: false,
            auto: true,
            pager: false,
            item: 4,
            pause: 10000,
            loop: true,
            responsive: [{
                breakpoint: 768,
                settings: {
                    item: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 1
                }
            }]
        });

        $(".slider-control._prev[data-switch='#certificates-slider']").click(function() {
            certificatesSlider.goToPrevSlide();
        });

        $(".slider-control._next[data-switch='#certificates-slider']").click(function() {
            certificatesSlider.goToNextSlide();
        });
    }

    if ($("#partners-slider").length != 0) {
        var partnersSlider = $("#partners-slider").lightSlider({
            controls: false,
            auto: true,
            pager: false,
            item: 4,
            pause: 10000,
            loop: true,
            responsive: [{
                breakpoint: 768,
                settings: {
                    item: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 1
                }
            }]
        });

        $(".slider-control._prev[data-switch='#partners-slider']").click(function() {
            partnersSlider.goToPrevSlide();
        });

        $(".slider-control._next[data-switch='#partners-slider']").click(function() {
            partnersSlider.goToNextSlide();
        });
    }

    if ($("#lawyer-deals").length != 0) {
        var lawyersDealsSlider = $("#lawyer-deals").lightSlider({
            controls: false,
            auto: true,
            pager: true,
            item: 2,
            pause: 10000,
            loop: true,
            slideMargin: 30,
            responsive: [{
                breakpoint: 768,
                settings: {
                    item: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 1
                }
            }]
        });

        $(".slider-control._prev[data-switch='#lawyer-deals']").click(function() {
            lawyersDealsSlider.goToPrevSlide();
        });

        $(".slider-control._next[data-switch='#lawyer-deals']").click(function() {
            lawyersDealsSlider.goToNextSlide();
        });
    }

    if ($("#lawyer-publications").length != 0) {
        var lawyersPublicationsSlider = $("#lawyer-publications").lightSlider({
            controls: false,
            auto: true,
            pager: true,
            item: 2,
            pause: 10000,
            loop: true,
            slideMargin: 30,
            responsive: [{
                breakpoint: 768,
                settings: {
                    item: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 1
                }
            }]
        });

        $(".slider-control._prev[data-switch='#lawyer-publications']").click(function() {
            lawyersPublicationsSlider.goToPrevSlide();
        });

        $(".slider-control._next[data-switch='#lawyer-publications']").click(function() {
            lawyersPublicationsSlider.goToNextSlide();
        });
    }

    $(".services-block--item").mouseenter(function() {
        var imageBlock = $(this).find(".services-block--item-img");
        var src = imageBlock.data("hover");
        imageBlock.css("background-image", `url(${src})`);
    })

    $(".services-block--item").mouseleave(function() {
        var imageBlock = $(this).find(".services-block--item-img");
        var src = imageBlock.data("src");
        imageBlock.css("background-image", `url(${src})`);
    })

    $(".services-switcher--item a").click(function(e) {
        e.preventDefault();
        var target = $(".services-block[data-content='" + $(this).data("target") + "']");
        $(".services-switcher--item").removeClass("_selected");
        $(".services-block").removeClass("_selected");
        $(this).parent().addClass("_selected");
        target.addClass("_selected");

    })

    $(document).scroll(function() {
        var scroll = $(document).scrollTop();
        if (scroll > $(window).height() && up.css("display") == "none") {
            up.fadeIn(300);
        } else if (scroll <= $(window).height() && up.css("display") == "block") {
            up.fadeOut(300);
        };
    })



}))
